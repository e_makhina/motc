package calculator.parser;

import calculator.lexer.Lexeme;
import calculator.lexer.Lexer;
import calculator.lexer.exceptions.LexerException;
import calculator.parser.exceptions.ParseException;

public class Parser {
    private Lexer lexer;
    private Lexeme nextLexeme;

    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }

    // TODO:
    // figure out what to do with exceptions
    public int calculate() throws ParseException, LexerException {
        nextLexeme = lexer.getLexeme();

        int result = parseExpr();
        if (nextLexeme.getLexType() != Lexeme.LexemeType.EOF) {
            throw new ParseException("Incorrect expression");
        }

        return result;
    }

    private int parseExpr() throws ParseException, LexerException {
        int total = parseTerm();

        while (nextLexeme.getLexType() == Lexeme.LexemeType.PLUS ||
                nextLexeme.getLexType() == Lexeme.LexemeType.MINUS) {
            int sign = 1;
            if (nextLexeme.getLexType() == Lexeme.LexemeType.MINUS) {
                sign = -1;
            }

            nextLexeme = lexer.getLexeme();
            total += sign * parseTerm();
        }

        return total;
    }

    private int parseTerm() throws ParseException, LexerException {
        int total = parseFactor();

        while (nextLexeme.getLexType() == Lexeme.LexemeType.MUL ||
                nextLexeme.getLexType() == Lexeme.LexemeType.DIV) {
            // flag shows if we should divide by next term
            boolean isInversed = false;
            if (nextLexeme.getLexType() == Lexeme.LexemeType.DIV) {
                isInversed = true;
            }

            nextLexeme = lexer.getLexeme();
            if (isInversed) {
                int factor = parseFactor();
                if (factor != 0) {
                    total /= factor;
                } else {
                    throw new ArithmeticException("Division by zero is forbidden");
                }
            } else {
                total *= parseFactor();
            }
        }

        return total;
    }

    private int parseFactor() throws ParseException, LexerException {
        int total = parsePower();

        if (nextLexeme.getLexType() == Lexeme.LexemeType.POW) {
            nextLexeme = lexer.getLexeme();
            int factor = parseFactor();
            total = (int) Math.pow(total, factor);
        }

        return total;
    }

    private int parsePower() throws ParseException, LexerException {
        int total = 0;
        int sign = 1;

        if (nextLexeme.getLexType() == Lexeme.LexemeType.MINUS) {
            sign = -1;
            nextLexeme = lexer.getLexeme();
        }

        total = sign * parseAtom();
        return total;
    }

    private int parseAtom() throws ParseException, LexerException {
        int total = 0;

        if (nextLexeme.getLexType() == Lexeme.LexemeType.NUM) {
            total = Integer.parseInt(nextLexeme.getLexSequence());
        } else if (nextLexeme.getLexType() == Lexeme.LexemeType.OPEN_BR) {
            nextLexeme = lexer.getLexeme();
            total = parseExpr();
            if (nextLexeme.getLexType() != Lexeme.LexemeType.CLOSE_BR) {
                throw new ParseException("Closing bracket expected");
            }
        } else {
            throw new ParseException("Incorrect expression");
        }

        nextLexeme = lexer.getLexeme();
//        System.out.println("atom " + total);
        return total;
    }
}
