package calculator.parser.exceptions;

public class ParseException extends Exception {
    public ParseException() {
        super();
    }

    public ParseException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
