package calculator.lexer;

import calculator.lexer.exceptions.LexerException;

import java.io.IOException;
import java.io.Reader;

public class Lexer {
    private Reader reader;
    private int lastLexeme = 0;
    // flag shows if last lexeme was used during previous iteration
    private boolean isLexemeUsed = true;

    public Lexer(Reader reader) {
        this.reader = reader;
    }

    public Lexeme getLexeme() throws LexerException {
        try {
            if (isLexemeUsed) {
                lastLexeme = reader.read();
            }

            while (Character.isSpaceChar(lastLexeme)) {
                lastLexeme = reader.read();
            }

            switch (lastLexeme) {
                case -1:
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.EOF, null);

                case '(':
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.OPEN_BR, "(");

                case ')':
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.CLOSE_BR, ")");

                case '+':
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.PLUS, "+");

                case '-':
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.MINUS, "-");

                case '*':
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.MUL, "*");

                case '/':
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.DIV, "/");

                case '^':
                    isLexemeUsed = true;
                    return new Lexeme(Lexeme.LexemeType.POW, "^");

                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    String lexSymbol = new String();
                    while (Character.isDigit(lastLexeme)) {
                        lexSymbol += (char) lastLexeme;
                        lastLexeme = reader.read();
                    }
                    isLexemeUsed = false;
                    return new Lexeme(Lexeme.LexemeType.NUM, lexSymbol);

                default:
                    throw new LexerException("Undefined lexeme");

            }
        } catch (IOException e) {
            throw new LexerException(e.getMessage());
        }
    }
}
