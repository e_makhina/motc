package calculator.lexer.exceptions;

public class LexerException extends Exception {
    public LexerException() {
        super();
    }

    public LexerException(String s) {
        super(s);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
