package calculator.lexer;

public class Lexeme {
    public enum LexemeType {
        NUM, OPEN_BR, CLOSE_BR, PLUS, MINUS, MUL, DIV, POW, EOF
    }

    private String lexSequence;
    private LexemeType lexType;

    public Lexeme(LexemeType lexType, String lexSequence) {
        this.lexType = lexType;
        this.lexSequence = lexSequence;
    }

    public String getLexSequence() {
        return lexSequence;
    }

    public LexemeType getLexType() {
        return lexType;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }

        return lexSequence.equals(((Lexeme) obj).getLexSequence());
    }

    @Override
    public int hashCode() {
        return lexSequence.hashCode();
    }
}
