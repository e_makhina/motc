package calculator;

import calculator.lexer.Lexeme;
import calculator.lexer.Lexer;
import calculator.lexer.exceptions.LexerException;
import org.testng.annotations.Test;

import java.io.StringReader;

public class LexerTest {
    @Test
    public void NumberTest() {
        Lexer lexer = new Lexer(new StringReader("554     0 7045786"));
        String arr[] = { "554", "0", "7045786"};
        int i = 0;
        try {
            Lexeme lexeme = lexer.getLexeme();
            while (lexeme.getLexType() != Lexeme.LexemeType.EOF) {
                assert lexeme.getLexSequence().equals(arr[i++]);
                lexeme = lexer.getLexeme();
            }

        } catch (LexerException e) {
                e.printStackTrace();
        }
    }

    @Test
    public void SymbolTest() {
        Lexer lexer = new Lexer(new StringReader("    +*/   ^ ( -)"));
        String arr[] = { "+", "*", "/", "^", "(", "-", ")"};
        int i = 0;
        try {
            Lexeme lexeme = lexer.getLexeme();
            while (lexeme.getLexType() != Lexeme.LexemeType.EOF) {
                assert lexeme.getLexSequence().equals(arr[i++]);
                lexeme = lexer.getLexeme();
            }

        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test(expectedExceptions = LexerException.class)
    public void IncorrectSymbolTest() throws LexerException {
        Lexer lexer = new Lexer(new StringReader("55^4 & 0 70@45786"));
        String arr[] = { "55", "^", "4", "0", "70", "45786"};
        int i = 0;
        Lexeme lexeme = lexer.getLexeme();
        while (lexeme.getLexType() != Lexeme.LexemeType.EOF) {
            assert lexeme.getLexSequence().equals(arr[i++]);
            lexeme = lexer.getLexeme();
        }
    }
}
