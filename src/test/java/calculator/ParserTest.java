package calculator;

import calculator.lexer.Lexer;
import calculator.lexer.exceptions.LexerException;
import calculator.parser.Parser;
import calculator.parser.exceptions.ParseException;
import org.testng.annotations.Test;

import java.io.StringReader;

public class ParserTest {
    @Test
    public void TermTest() {
        try {
            Lexer lexer = new Lexer(new StringReader("55+7  - 150"));
            Parser parser = new Parser(lexer);
            int res = parser.calculate();
            assert res == -88;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void FactorTest() {
        try {
            Lexer lexer = new Lexer(new StringReader("  887 * 5-896541 /698"));
            Parser parser = new Parser(lexer);
            int res = parser.calculate();
            assert res == 3151;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void PowerTestOne() {
        try {
            Lexer lexer = new Lexer(new StringReader("2^3^2"));
            Parser parser = new Parser(lexer);
            int res = parser.calculate();
            assert res == 512;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void PowerTestTwo() {
        try {
            Lexer lexer = new Lexer(new StringReader("(2^3)^2"));
            Parser parser = new Parser(lexer);
            int res = parser.calculate();
            assert res == 64;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void PowerTestTrhee() {
        try {
            Lexer lexer = new Lexer(new StringReader("-2^2"));
            Parser parser = new Parser(lexer);
            int res = parser.calculate();
            assert res == 4;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void AtomTest() {
        try {
            Lexer lexer = new Lexer(new StringReader("2 + (5*10 )"));
            Parser parser = new Parser(lexer);
            int res = parser.calculate();
            assert res == 52;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void ExpressionTest() {
        try {
            Lexer lexer = new Lexer(new StringReader("2^3^2 * (80/5*4 - 256/(2^3))"));
            Parser parser = new Parser(lexer);
            int res = parser.calculate();
            assert res == 16384;

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (LexerException e) {
            e.printStackTrace();
        }
    }

    @Test(expectedExceptions = ParseException.class)
    public void ExceptionTestBrackets() throws LexerException, ParseException {
        Lexer lexer = new Lexer(new StringReader("2^3^2 * () (80/5*4 - 256/(2^3))"));
        Parser parser = new Parser(lexer);
        int res = parser.calculate();
        assert res == 16384;
    }

    @Test(expectedExceptions = ParseException.class)
    public void MissingBracketTest() throws LexerException, ParseException {
        Lexer lexer = new Lexer(new StringReader("2^3^2 * ((( (80/5*4 - 256/(2^3))"));
        Parser parser = new Parser(lexer);
        int res = parser.calculate();
        assert res == 16384;
    }
}
